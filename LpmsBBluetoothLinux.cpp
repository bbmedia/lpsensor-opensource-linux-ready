/***********************************************************************
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

#include "LpmsBBluetooth.h"


LpmsBBluetooth::LpmsBBluetooth(CalibrationData *configData) :
	LpmsIoInterface(configData)
{
    close();

    // ADI return 0;
}

LpmsBBluetooth::~LpmsBBluetooth(void)
{

}
	
long long LpmsBBluetooth::getConnectWait(void) 
{ 
	return 0; 
}

void LpmsBBluetooth::listDevices(LpmsDeviceList *deviceList)
{
	(void) deviceList;
}

void LpmsBBluetooth::stopDiscovery(void)
{
}

bool LpmsBBluetooth::Connect(string deviceId)
{	
    struct sockaddr_rc addr = { 0 };
	int s;

	bzSocket = socket(AF_BLUETOOTH, SOCK_STREAM, BTPROTO_RFCOMM);

	addr.rc_family = AF_BLUETOOTH;
	addr.rc_channel = (uint8_t) 1;
	str2ba(deviceId.c_str(), &addr.rc_bdaddr);


    //Adi status = connect(bzSocket, (struct sockaddr *)&addr, sizeof(addr));
    return connect(bzSocket, (struct sockaddr *)&addr, sizeof(addr));;
}

void LpmsBBluetooth::close(void)
{
}	

bool LpmsBBluetooth::Read(char *rxBuffer, unsigned long *bytesReceived)
{
	*bytesReceived = read(bzSocket, rxBuffer, sizeof(rxBuffer));

	return true;
}

bool LpmsBBluetooth::Write(char *txBuffer, unsigned bufferLength)
{	
	int s;

    // ADI
    s = write(bzSocket, txBuffer , bufferLength);

	return true;
}

bool LpmsBBluetooth::sendModbusData(unsigned address, unsigned function, unsigned length, unsigned char *data)
{
    char txData[1024];
	unsigned int txLrcCheck;
	
	if (length > 1014) return false;

	txData[0] = 0x3a;
	txData[1] = address & 0xff;
	txData[2] = (address >> 8) & 0xff;
	txData[3] = function & 0xff;
	txData[4] = (function >> 8) & 0xff;
	txData[5] = length & 0xff;
	txData[6] = (length >> 8) & 0xff;
	
	for (unsigned int i=0; i < length; ++i) {
		txData[7+i] = data[i];
	}
	
	txLrcCheck = address;
	txLrcCheck += function;
	txLrcCheck += length;
	
	for (unsigned int i=0; i < length; i++) {
		txLrcCheck += data[i];
	}
	
	txData[7 + length] = txLrcCheck & 0xff;
	txData[8 + length] = (txLrcCheck >> 8) & 0xff;
	txData[9 + length] = 0x0d;
	txData[10 + length] = 0x0a;
	

    if (Write(txData, length+11) == true) {
		return true;
	}
	
	return false;
}

bool LpmsBBluetooth::parseModbusByte(unsigned char b)
{
	(void) b;

	return true;
}

bool LpmsBBluetooth::pollData(void) 
{
	return true;
}

bool LpmsBBluetooth::deviceStarted(void)
{
	return true;
}
