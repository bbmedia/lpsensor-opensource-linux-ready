/***********************************************************************
** Copyright (C) 2013 LP-Research
** All rights reserved.
** Contact: LP-Research (info@lp-research.com)
***********************************************************************/

#ifndef LPMS_IO_INTERFACE
#define LPMS_IO_INTERFACE

#include "ImuData.h"
#include "CalibrationData.h"
#include "LpmsRegisterDefinitions.h"
#include "LpMatrix.h"
#include "MicroMeasure.h"

#ifdef _WIN32
	#include "windows.h"
#endif

#include <iostream>
#include <string>
#include <queue>
#include <fstream>

#include <boost/cstdint.hpp>

#define PACKET_ADDRESS0 0
#define PACKET_ADDRESS1 1
#define PACKET_FUNCTION0 2
#define PACKET_FUNCTION1 3
#define PACKET_RAW_DATA 4
#define PACKET_LRC_CHECK0 5
#define PACKET_LRC_CHECK1 6
#define PACKET_END 7
#define PACKET_LENGTH0 8
#define PACKET_LENGTH1 9
#define PACKET_SKIP_ZERO 10

#define FIRMWARE_PACKET_LENGTH 256
#define IDLE_STATE -1
#define ACK_MAX_TIME 200000

typedef union _float2uint {
	float fp;
	boost::uint32_t up;
} float2uint;

/* Class for low-level communication with LPMS devices. */
class LpmsIoInterface {
public:
	LpmsIoInterface(CalibrationData *configData);
    virtual bool Connect(std::string deviceId) ;
	virtual bool deviceStarted(void);
	virtual void loadData(ImuData *data);
	virtual bool pollData(void);
	virtual void close(void);
	virtual void startStreaming(void);
	virtual void stopStreaming(void);
	virtual long long getConnectWait(void);
	virtual float getSamplingTime(void);
	virtual int getGyroCalCycles(void);

	bool checkState(void);
	int getCurrentState(void);
	CalibrationData *getConfigData(void);
	bool isWaitForData(void);
	bool isWaitForAck(void);
	bool isCalibrating(void);
	bool isError(void);	
	bool getUploadProgress(int *p);
	bool startUploadFirmware(std::string fn);
	bool startUploadIap(std::string fn);
	bool setCommandMode(void);
	bool setStreamMode(void);
	bool setSleepMode(void);		
	bool restoreFactoryValue(void);
	bool setSelfTest(long v);
	bool selectData(long p);
	bool setImuId(long v);
	bool setBaudrate(long v);
	bool setStreamFrequency(long v);
	bool startGyrCalibration(void);
	bool startAccCalibration(void);
	bool startMagCalibration(void);
	bool setGyrRange(long v);
	bool setMagRange(long v);
	bool setAccRange(long v);
	bool setGyrOutputRate(long v);
	bool setMagOutputRate(long v);
	bool setAccOutputRate(long v);
	bool setGyrBias(long x, long y, long z);
	bool setAccBias(LpVector3f v);
	bool setMagBias(long x, long y, long z);
	bool setAccRef(float x, float y, float z);
	bool setMagRef(float x, float y, float z);
	bool setGyrThres(float x, float y, float z);
	bool setAccCovar(float v);
	bool setAccCompGain(float v);
	bool setMagCovar(float v);
	bool setMagCompGain(float v);
	bool setProcessCovar(float v);
	bool resetFilterParam(void);
	bool setFilterMode(long v);
	bool setFilterPreset(long v);
	bool getFirmwareVersion(void);
	bool getDeviceId(void);
	bool getDeviceReleaseDate(void);
	bool getConfig(void);
	bool getImuId(void);
	bool getStatus(void);
	bool getBaudrate(void);
	bool getStreamFreq(void);
	bool getGyrRange(void);
	bool getAccRange(void);
	bool getMagRange(void);
	bool getGyrOutputRate(void);
	bool getAccOutputRate(void);
	bool getMagOutputRate(void);
	bool getGyrBias(void);
	bool getAccBias(void);
	bool getMagBias(void);
	bool getAccRef(void);
	bool getMagRef(void);
	bool getGyrThres(void);
	bool getAccCovar(void);
	bool getAccCompGain(void);
	bool getMagCovar(void);
	bool getMagCompGain(void);
	bool getProcessCovar(void);
	bool getSensorData(void);
	bool getFilterMode(void);
	bool getFilterPreset(void);	
	bool resetOrientation(void);
	bool resetReference(void);
	bool enableGyrThres(long v);
	bool writeRegisters(void);
	bool enableMagAutocalibration(long v);
	int getMode(void);
	bool setCanStreamFormat(long v);
	bool setCanBaudrate(long v);
	float getLatestLatency(void);
	bool parseFieldMapData(void);
	bool getHardIronOffset(void);
	bool getSoftIronMatrix(void);
	bool setHardIronOffset(void);
	bool setSoftIronMatrix(void);
	bool enableGyrAutocalibration(long v);
	void zeroImuData(ImuData* id);
	bool setHardIronOffset(LpVector3f v);
	bool setSoftIronMatrix(LpMatrix3x3f m);
	bool setFieldEstimate(float v);
	bool getFieldEstimate(void);
	bool setAccAlignment(LpMatrix3x3f m);
	bool getAccAlignment(void);
	bool setGyrAlignment(LpMatrix3x3f m);
	bool setGyrAlignmentBias(LpVector3f v);
	bool setGyrTempCalPrmA(LpVector3f v);
	bool setGyrTempCalPrmB(LpVector3f v);
	bool setGyrTempCalBaseV(LpVector3f v);
	bool setGyrTempCalBaseT(float v);
	bool getGyrAlignment(void);
	bool getGyrAlignmentBias(void);
	bool getGyrTempCalPrmA(void);
	bool getGyrTempCalPrmB(void);
	bool getGyrTempCalBaseV(void);
	bool getGyrTempCalBaseT(void);
	long getConfigReg(void);
	bool isNewData(void);
	bool getRawDataLpFilter(void);
	bool setRawDataLpFilter(int v);
	bool getCanMapping(void);
	bool setCanMapping(int *v);
	bool getCanHeartbeat(void);
	bool setCanHeartbeat(int v);
	bool resetTimestamp(void);
	bool getLinAccCompMode(void);
	bool setLinAccCompMode(int v);
	bool getCentriCompMode(void);
	bool setCentriCompMode(int v);
	bool getCanConfiguration(void);
	bool setCanChannelMode(int v);
	bool setCanPointMode(int v);
	bool setCanStartId(int v);
	
protected:
	virtual bool sendModbusData(unsigned address, unsigned function, unsigned length, unsigned char *data);
	virtual bool parseModbusByte(unsigned char b);

	bool isAck(void);
	bool isNack(void);
	void receiveReset(void);
	bool parseFunction(void);
	bool handleFirmwareFrame(void);
	bool handleIAPFrame(void);
	boost::uint32_t conFtoI(float f);
	float conItoF(boost::uint32_t v);
	bool modbusSetNone(unsigned command);
	bool modbusGet(unsigned command);	
	bool modbusGetMultiUint32(unsigned command, boost::uint32_t *v, int n); 
	bool modbusSetInt32(unsigned command, long v);
	bool modbusSetInt32Array(unsigned command, long *v, int length);
	bool modbusSetVector3Int32(unsigned command, long x, long y, long z);
	bool modbusSetFloat(unsigned command, float v);
	bool modbusSetVector3Float(unsigned command, float x, float y, float z);
	bool fromBuffer(std::vector<unsigned char> data, long *v);
	bool fromBuffer(std::vector<unsigned char> data, long *x, long *y, long *z);
	bool fromBuffer(std::vector<unsigned char> data, float *v);
	bool fromBuffer(std::vector<unsigned char> data, float *x, float *y, float *z);
	bool fromBuffer(std::vector<unsigned char> data, long *v, int length);
	bool fromBuffer(std::vector<unsigned char> data, unsigned start, float *x, float *y, float *z);
	bool fromBuffer(std::vector<unsigned char> data, unsigned start, float *q0, float *q1, float *q2, float *q3);
	bool fromBuffer(std::vector<unsigned char> data, unsigned start, float *v);
	bool fromBuffer(unsigned char *data, float *v);
	bool fromBufferBigEndian(unsigned char *data, float *v);
	bool parseSensorData(void);
	bool modbusSetMatrix3x3f(unsigned command, LpMatrix3x3f m);
	bool modbusSetVector3f(unsigned command, LpVector3f v);

	int fieldMapPitch;
	int fieldMapRoll;
	int fieldMapYaw;
	int currentFieldMapPitch;
	int currentFieldMapRoll;
	int currentFieldMapYaw;
	unsigned currentAddress;
	unsigned currentFunction;
	unsigned currentLength;
	std::queue<unsigned char> dataQueue;
	std::vector<unsigned char> oneTx;
	unsigned lrcIndex;
	unsigned lrcCheck;
	unsigned lrcReceived;
	int rxState;
	int rawDataIndex;	
	bool waitForAck;
	bool ackReceived;
	int currentState;	
	long ackTimeout;
	bool waitForData;
	bool dataReceived;
	long dataTimeout;
	int pCount;
	ImuData imuData;
	CalibrationData *configData;
	std::ifstream ifs;	
	long configReg;
	long lpmsStatus;
	long imuId;
	long long firmwarePages;
	int currentMode;
	bool configSet;
	float latestLatency;
	MicroMeasure latencyTimer;
	bool newDataFlag;
};	

#endif
